package QNR;

use Mojo::Base 'Mojolicious';

# This method will run once at server start
sub startup {
  my $self = shift; 

  # Documentation browser under "/perldoc"
  $self->plugin('PODRenderer');

  # Router
  my $r = $self->routes;

  # Normal route to controller
  $r->get('/')->to('index.html');

  $r->get('/main')->to('index.html');
  $r->get('/question/:q')->to( controller => 'controller', action => 'getquestion' );

}

1;
