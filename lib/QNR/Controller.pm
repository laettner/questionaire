package QNR::Controller;

use FindBin;
use Data::Dumper;

BEGIN { 
	push @INC, "$FindBin::Bin/../../../common/",
			   "$FindBin::Bin/../../../common/utils/",
			   "$FindBin::Bin/../",
			   "$FindBin::Bin/../enum/";
}; 

use Mojo::Base 'Mojolicious::Controller';
use IO::Compress::Gzip 'gzip';
use IO::File;
use JSON -convert_blessed_universally;

use Framework::ContextFactory;
use Framework::EnvVariableParser;
use Framework::Logger;
use Framework::ReturnMessage;

use utf8;
use Encode qw/encode decode/;

$ENV{'NLS_DATE_FORMAT'} = 'YYYY-MM-DD HH24:MI:SS';
$ENV{'NLS_LANG'} = 'AMERICAN_AMERICA.UTF8';

my $dataFilePath = "$FindBin::Bin/qdata.json";
my $json_text = Config::JSON->new($dataFilePath); 

sub getquestion {
	my $self = shift; 
	my %params = (
		qnum => $self->param('q'),
	);
	my $data = $json_text->{config};
	$self->render(json =>  $data->{$params{qnum}});
}

sub _trim {
	my ($line) = @_;
	$line =~ s/\n$//;
	$line =~ s/\r$//;
	$line =~ s/\R//;
	
	return $line;
}

sub _renderJsonSuccess {
	my $self = shift;
	my ($response, $specialKey) = @_;
	my $responseKey = 'response';
	if ($specialKey) {
		$responseKey = $specialKey;
	}
	$self->render(json => {status => Mojo::JSON->true,
				 		   $responseKey => $response});
}

sub _renderJsonError {
	my $self = shift;
	my ($errorMessage) = @_;
	$errorMessage = $self->_maskErrorMessage($errorMessage);
	$self->render(json => {status => Mojo::JSON->false,
				 		   response => $errorMessage});
}

sub _invertRequestLowerCase {
	my $self = shift;
	my ($jsonData) = @_;
	my (@jsonKeys) = $jsonData =~ /[A-Z_"]{2,}:/g;
	foreach my $currKey (@jsonKeys) {
		my $lcKey = lc($currKey);
		$jsonData =~ s/$currKey/$lcKey/;
	}
	return encode('UTF-8',$jsonData);
}

1;
